const request = require('request');

const config = {
    clientId: 'b1cd060d8fe4b3a30a76',
    clientSecret: process.env.clientSecret,
    redirectUri: 'GITHUB REDIRECT URI',// currently unused: using the origin to allow nmaster/canary/dev to use the same clud function
    allowedOrigins: ['https://github.fileprovider.flast.org', 'https://github.fileprovider.flast.org/canary','https://github.fileprovider.flast.org/dev'],
};

exports.github = (req, res) => {
  	
    const headers = req.headers;
    const origin = headers.origin || headers.Origin;

    // Check for malicious request
    if (!config.allowedOrigins.includes(origin)) {
        throw new Error(`${headers.origin} is not an allowed origin.`);
    }

  	res.set('Access-Control-Allow-Origin', origin);
            
    res.set('Access-Control-Allow-Methods', 'GET');
    res.set('Access-Control-Allow-Headers', 'Content-Type,Accept');
    res.set('Access-Control-Max-Age', '3600');

    const code = req.query.code;
    const state = req.query.state;
    const url = `https://github.com/login/oauth/access_token?client_id=${config.clientId}&client_secret=${config.clientSecret}&code=${code}&redirect_uri=${origin}&state=${state}`;
    const options = {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }
    };

    // Request to GitHub with the given code
    request(url, options, function(err, response) {
        if (err) {
            res.status(err).send(err);
            console.Error(`${err}: ${response}`);
            return false;
        }
        res.status(200).send(response.body);
        return true;
    });
};