console.log("github fileProvider");

const MODULE_NAME = "fileProvider";

let token;

const CLIENT_ID = "b1cd060d8fe4b3a30a76";
const STATE = "YOUR_UNIQUE_STATE_HASH";

let urlParams = new URLSearchParams(window.location.search);
let code = urlParams.get('code');

let bc = new BroadcastChannel('token_channel');

//is the redirected new tab and has code?
if(code) {
    bc.postMessage(code);
    window.close();
}
//is the original tab and has token saved
else if(sessionStorage.getItem("token")) {
    token = sessionStorage.getItem("token");
    getUser();
    getUserProjects();
}

bc.onmessage = function (e) { 
    code = e.data;
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function(){
        const res = JSON.parse(this.responseText);
        token = res.access_token;
        sessionStorage.setItem("token",token);
        getUser();
        getUserProjects();
    });
    oReq.open("GET", `https://europe-west1-zinc-citron-220314.cloudfunctions.net/github?code=${code}&state=${STATE}`);
    oReq.send();
}

document.getElementById("btnLogin").addEventListener("click",e => {
    let w = window.open( `https://github.com/login/oauth/authorize?client_id=${CLIENT_ID}&redirect_uri=${window.location.href}&state=${STATE}&scope=repo`,'_blank');
    w.focus();
});


function getUser() {
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function(){
        console.log(this.responseText);
    });
    oReq.open("GET", `https://api.github.com/user`);
    oReq.setRequestHeader("Authorization", `token ${token}`);
    oReq.send();
}

function getUserProjects() {
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function(){console.log(this.responseText)});
    oReq.open("GET", `https://api.github.com/user/repos`);
    oReq.setRequestHeader("Authorization", `token ${token}`);
    oReq.send();
}

function getBranchesInRepository() {
    const owner = document.getElementById("inputOwner").value;
    const repo = document.getElementById("inputRepo").value;
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", getBranchesInProjectListener);
    oReq.open("GET", `https://api.github.com/repos/${owner}/${repo}/branches`);
    oReq.setRequestHeader("Authorization", `token ${token}`);
    oReq.send();
}

function getBranchesInProjectListener() {
    let select = document.getElementById("selectBranch");
    while (select.firstChild) {
        select.removeChild(select.firstChild);
    }
    let option = document.createElement("option");
    option.value = "--select branch--";
    option.text = "--select branch--";
    select.appendChild(option);
    JSON.parse(this.responseText).forEach(branch => {
        let option = document.createElement("option");
        option.value = branch.name;
        option.text = branch.name;
        select.appendChild(option);
    });
}

let selectBranch = document.getElementById("selectBranch");
selectBranch.addEventListener("change", e => {
    if(selectBranch.firstChild.value === "--select branch--")
        selectBranch.removeChild(selectBranch.firstChild);
    emitEvent("fileProviderReady",{branch:selectBranch.value});
});

let selectRepo = document.getElementById("btnRepoSelection");
selectRepo.addEventListener("click", e => {
    getBranchesInRepository();
});

cmds["getFile"] = getFile;
function getFile(m) {
    const owner = document.getElementById("inputOwner").value;
    const repo = document.getElementById("inputRepo").value;
    const branch = document.getElementById("selectBranch").value;
    const path = m.file_path || "";
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function() {
        getFileCallback(m,this.responseText);
    });
    oReq.open("GET", `https://api.github.com/repos/${owner}/${repo}/contents/${encodeURIComponent(path)}?ref=${branch}`);
    oReq.setRequestHeader("Authorization", `token ${token}`);    
    oReq.send();
}

function getFileCallback(m,res) {
    //might want to pass more info about file, ie last modified...
    m.callback.data = {content:b64DecodeUnicode(JSON.parse(res).content)};
    messageModule(m.callback,m.callback.target);
}

cmds["getFilesInDirectory"] = getFilesInDirectory;
function getFilesInDirectory(m) {
    let path = m.path || "";
    const branch = document.getElementById("selectBranch").value;
    const owner = document.getElementById("inputOwner").value;
    const repo = document.getElementById("inputRepo").value;
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function(){getFilesInDirectoryListener(m,container,this.responseText);});
    oReq.open("GET", `https://api.github.com/repos/${owner}/${repo}/contents/${encodeURIComponent(path)}?ref=${branch}`);
    oReq.setRequestHeader("Authorization", `token ${token}`);
    oReq.send();
}

let fileList = [];

function getFilesInDirectoryListener (m,container,json) {
    let files = JSON.parse(json);
    files.forEach(f => {
        fileList.push(f);
        f.originalPath = f.path;
        if(f.type === "dir")
            f.type = "tree";
        else if(f.type === "file")
            f.type = "blob";
    });
    m.callback.data = {files:files};
    messageModule(m.callback,m.callback.target);
}


//TODO: use github data API to reduce number of commits
//https://developer.github.com/v3/git/
//commits using a blob sha need to get run one after another or they interfere
cmds["commit"] = commit;
function commit(m) {
    const owner = document.getElementById("inputOwner").value;
    const repo = document.getElementById("inputRepo").value;
    const branch = m.branch || document.getElementById("selectBranch").value;
    const message = m.commit_message;
    commitTime = Date.now();
    let actionIndex = 0;
    function commitAction(action) {
        let path = action.file_path;
        const content = action.content;
        let payload = {
            message: message,
            content: btoa(content),
            branch: branch
        };
        let oReq = new XMLHttpRequest();
        oReq.addEventListener("load", function(){
            actionIndex++;
            if(m.actions.length > actionIndex)
                commitAction(m.actions[actionIndex]);
            else
                emitEvent("confirmCommit",{res:this.responseText,commitNumber:m.commitNumber,doMergeRequest:m.doMergeRequest});
        });
        oReq.addEventListener("error", e => emitEvent("commitError",{error:e,commitNumber:m.commitNumber}));
        if(action.action === "create") {
            oReq.open("PUT", `https://api.github.com/repos/${owner}/${repo}/contents/${path}`);
            oReq.setRequestHeader("Authorization", `token ${token}`);
            oReq.send(JSON.stringify(payload));
        }
        else {
            let oReq3 = new XMLHttpRequest();
            oReq3.addEventListener("error",e => console.error(e));
            oReq3.addEventListener("load",function() {
                const sha = JSON.parse(this.responseText).sha;
                payload.sha = sha;
                let oReqAction;
                if(action.action === "update") {
                    oReqAction = "PUT";

                    oReq.open(oReqAction, `https://api.github.com/repos/${owner}/${repo}/contents/${path}`);
                    oReq.setRequestHeader("Authorization", `token ${token}`);
                    oReq.send(JSON.stringify(payload));
                }
                else if(action.action === "delete") {
                    oReqAction = "DELETE";
                    delete payload.content;

                    oReq.open(oReqAction, `https://api.github.com/repos/${owner}/${repo}/contents/${path}`);
                    oReq.setRequestHeader("Authorization", `token ${token}`);
                    oReq.send(JSON.stringify(payload));
                }
                else if(action.action === "move") {
                    oReqAction = "PUT";
                    
                    let oReq2 = new XMLHttpRequest();
                    oReq2.addEventListener("load", function() {
                        oReq.open(oReqAction, `https://api.github.com/repos/${owner}/${repo}/contents/${path}`);
                        oReq.setRequestHeader("Authorization", `token ${token}`);
                        oReq.send(JSON.stringify(payload));
                    });
                    oReq2.open("DELETE", `https://api.github.com/repos/${owner}/${repo}/contents/${action.previous_path}`);
                    oReq2.setRequestHeader("Authorization", `token ${token}`);
                    oReq2.send(JSON.stringify({path:action.previous_path,message:message,sha:sha,branch:branch}));
                }
            });
            let encodedPath = path;
            if(action.action === "move")
                encodedPath = action.previous_path;
            encodedPath = encodeURIComponent(encodedPath);
            oReq3.open("GET", `https://api.github.com/repos/${owner}/${repo}/contents/${encodedPath}?ref=${branch}`);
            oReq3.setRequestHeader("Authorization", `token ${token}`);
            oReq3.send();
        }
    }
    
    commitAction(m.actions[0]);
}















//from https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding
function b64DecodeUnicode(str) {
    return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(''))
}